* ```npx create-react-app consolidado```

* ```cd consolidado```

* ```git init```

* ```npm install react-router-dom@6 styled-components```

* ```git status```

* ```git add .```

* ```git commit -m 'configuracao inicial'```

* ```git tag -a v0.1 -m 'configuracao inicial'```

-- abrir o folder no vscode

-- apagar o conteúdo do arquivo App.css

-- no arquivo App.js, substituir o conteúdo do elemento div className="App" por Vazio

-- criar os folders components e pages

-- conferir o resultado no navegador

* ```npm start```

-- para o servidor com CTRL+c

-- verificar o estado do reposititório com git status 

-- o git não controla diretórios vazios

* ```git add .```

* ```git commit -m 'projeto vazio'```

* ```git tag -a v0.2 -m 'projeto vazio'```

-- listar as tag com git tag

-- sair com CTRL+q

-- voltar no tempo 

* ```git checkout v0.1```

-- andar pra frente

* ```git switch -```

- criar uma nova página
- criar uma rota para uma nova página
- criar um componente

* ```git status```

* ```git add .```

* ```git status```

-- criar uma tag _componentes iniciais_

-- fazer o componente receber um nome via props

-- criar uma tag _props_

-- criar um arquivo json

-- ler o arquivo json na página _Exemplo_

-- fazer um loop pelo arquivo enviando seus dados para o componente via props

-- fazer a tag _arquivo json_

-- criar um repositório no gitlab !!!!! (não criar um README)

-- incluir o servidor remoto no git

* ```git remote add origin https://gitlab.com/EduardoMangeli/consolidado.git```

* ```git push --set-upstream origin main```

-- enviar as tags

* ```git push origin --tags```

-- ajustar o README do projeto no gitlab

