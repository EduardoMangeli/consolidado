import { Link } from "react-router-dom";
import Primeiro from "../components/primeiro/Primeiro";

import dados from '../dados.json';

const Exemplo = () => {
  return(
    <div>
      <h1>Minha página de exemplo</h1>
      { dados.map( (pessoa) => 
        <Primeiro 
          nome={pessoa.nome}
          cpf={pessoa.cpf}
        />
      )}
      
      <Link to="/">Home</Link>
    </div>
  );
}

export default Exemplo;