import { MinhaDiv, MeuP } from "./Style";

const Primeiro = ( props ) => {
  return(
    <MinhaDiv>
      <MeuP>{ props.nome }</MeuP>
      {props.cpf}
    </MinhaDiv>
  )
}

export default Primeiro;