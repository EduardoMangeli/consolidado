import styled from "styled-components";

const MinhaDiv = styled.div`
  color: brown;
  background-color: aqua;
`;

const MeuP = styled.p`
  font-style: italic;
  text-transform: capitalize;
`;

export { MinhaDiv, MeuP };